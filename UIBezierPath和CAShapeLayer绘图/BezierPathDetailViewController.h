//
//  BezierPathDetailViewController.h
//  UIBezierPath和CAShapeLayer绘图
//
//  Created by LeeJay on 16/9/28.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, BezierPathType) {
    BezierPathType_Triangle = 0,     // 三角形
    BezierPathType_Rect,        // 矩形
    BezierPathType_Circle,          // 圆
    BezierPathType_Oval,            // 椭圆
    BezierPathType_RoundedRect,     // 带圆角的矩形
    BezierPathType_Arc,             // 弧
    BezierPathType_SecondBezierPath,// 二次贝塞尔曲线
    BezierPathType_ThridBezierPath  // 三次贝塞尔曲线
};

@interface BezierPathDetailViewController : UIViewController

@property (nonatomic, assign) BezierPathType bezierType;

@end
