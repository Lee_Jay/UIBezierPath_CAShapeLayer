//
//  main.m
//  UIBezierPath和CAShapeLayer绘图
//
//  Created by LeeJay on 16/9/26.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
