//
//  BezierPathDetailView.m
//  UIBezierPath和CAShapeLayer绘图
//
//  Created by LeeJay on 16/9/28.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import "BezierPathDetailView.h"

@implementation BezierPathDetailView

- (void)drawRect:(CGRect)rect
{
    switch (_type) {
            
        case 0:// 三角形
        {
            [self drawTrianglePath];
        }
            break;
            
        case 1:// 矩形
        {
            [self drawRectPath];
        }
            break;

        case 2:// 圆
        {
            [self drawCiclePath];
        }
            break;

        case 3:// 椭圆
        {
            [self drawOvalPath];
        }
            break;

        case 4:// 带圆角的矩形
        {
            [self drawRoundedRectPath];
        }
            break;

        case 5:// 弧
        {
            [self drawARCPath];
        }
            break;

        case 6:// 二次贝塞尔曲线
        {
            [self drawSecondBezierPath];
        }
            break;
            
        case 7:// 三次贝塞尔曲线
        {
            [self drawThirdBezierPath];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - 画三角形

- (void)drawTrianglePath
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(20, 20)];
    [path addLineToPoint:CGPointMake(self.frame.size.width - 40, 20)];
    [path addLineToPoint:CGPointMake(self.frame.size.width / 2, self.frame.size.height - 20)];
    
    // 最后的闭合线是可以通过调用closePath方法来自动生成的，也可以调用-addLineToPoint:方法来添加
    //  [path addLineToPoint:CGPointMake(20, 20)];
    
    [path closePath];
    
    // 设置线宽
    path.lineWidth = 1.5;
    
    // 设置填充颜色
    UIColor *fillColor = [UIColor redColor];
    [fillColor set];
    [path fill];
    
    // 设置画笔颜色
    UIColor *strokeColor = [UIColor greenColor];
    [strokeColor set];
    
    // 根据我们设置的各个点连线
    [path stroke];
}

#pragma mark - 画矩形

- (void)drawRectPath
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:CGRectMake(20, 20, self.frame.size.width - 40, self.frame.size.height - 40)];
    
    path.lineWidth = 1.5;
    path.lineCapStyle = kCGLineCapRound;
    path.lineJoinStyle = kCGLineJoinBevel;
    
    // 设置填充颜色
    UIColor *fillColor = [UIColor redColor];
    [fillColor set];
    [path fill];
    
    // 设置画笔颜色
    UIColor *strokeColor = [UIColor greenColor];
    [strokeColor set];
    
    // 根据我们设置的各个点连线
    [path stroke];
}

#pragma mark - 画圆

- (void)drawCiclePath
{
    // 传的是正方形，因此就可以绘制出圆了
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(20, 20, self.frame.size.width - 40, self.frame.size.width - 40)];
    
    // 设置填充颜色
    UIColor *fillColor = [UIColor redColor];
    [fillColor set];
    [path fill];
    
    // 设置画笔颜色
    UIColor *strokeColor = [UIColor greenColor];
    [strokeColor set];
    
    // 根据我们设置的各个点连线
    [path stroke];
}

#pragma mark - 画椭圆

- (void)drawOvalPath
{
    // 传的是不是正方形，因此就可以绘制出椭圆圆了
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(20, 20, self.frame.size.width - 80, self.frame.size.height - 40)];
    
    // 设置填充颜色
    UIColor *fillColor = [UIColor redColor];
    [fillColor set];
    [path fill];
    
    // 设置画笔颜色
    UIColor *strokeColor = [UIColor greenColor];
    [strokeColor set];
    
    // 根据我们设置的各个点连线
    [path stroke];
}

#pragma mark - 画带圆角的矩形

- (void)drawRoundedRectPath
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(20, 20, self.frame.size.width - 40, self.frame.size.height - 40) byRoundingCorners:UIRectCornerTopRight cornerRadii:CGSizeMake(20, 20)];
    // 设置填充颜色
    UIColor *fillColor = [UIColor redColor];
    [fillColor set];
    [path fill];
    
    // 设置画笔颜色
    UIColor *strokeColor = [UIColor greenColor];
    [strokeColor set];
    
    // 根据我们设置的各个点连线
    [path stroke];
}

#pragma mark - 画弧

- (void)drawARCPath
{
    CGPoint center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center
                                                        radius:100
                                                    startAngle:0
                                                      endAngle:((M_PI * 135)/ 180)
                                                     clockwise:YES];
    
    path.lineCapStyle = kCGLineCapRound;
    path.lineJoinStyle = kCGLineJoinRound;
    path.lineWidth = 5.0;
    
    UIColor *strokeColor = [UIColor redColor];
    [strokeColor set];
    
    [path stroke];
}

#pragma mark - 画二次贝塞尔曲线

- (void)drawSecondBezierPath
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    // 首先设置一个起始点
    [path moveToPoint:CGPointMake(20, self.frame.size.height - 100)];
    
    // 添加二次曲线
    [path addQuadCurveToPoint:CGPointMake(self.frame.size.width - 20, self.frame.size.height - 100)
                 controlPoint:CGPointMake(self.frame.size.width / 2, 0)];
    
    path.lineCapStyle = kCGLineCapRound;
    path.lineJoinStyle = kCGLineJoinRound;
    path.lineWidth = 5.0;
    
    UIColor *strokeColor = [UIColor redColor];
    [strokeColor set];
    
    [path stroke];
}

#pragma mark - 画三次贝塞尔曲线

- (void)drawThirdBezierPath
{
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    // 设置起始端点
    [path moveToPoint:CGPointMake(20, 150)];
    
    [path addCurveToPoint:CGPointMake(300, 150)
            controlPoint1:CGPointMake(160, 0)
            controlPoint2:CGPointMake(160, 250)];
    
    path.lineCapStyle = kCGLineCapRound;
    path.lineJoinStyle = kCGLineJoinRound;
    path.lineWidth = 5.0;
    
    UIColor *strokeColor = [UIColor redColor];
    [strokeColor set];
    
    [path stroke];
}

@end
