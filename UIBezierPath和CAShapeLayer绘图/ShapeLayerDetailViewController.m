//
//  ShapeLayerDetailViewController.m
//  UIBezierPath和CAShapeLayer绘图
//
//  Created by LeeJay on 16/9/28.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import "ShapeLayerDetailViewController.h"

@interface ShapeLayerDetailViewController ()

@property (nonatomic, strong) CAShapeLayer *loadingLayer;

@end

@implementation ShapeLayerDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
}

/**
 这边主要举了两个例子，矩形、圆，感兴趣的同学可以根据前面的贝塞尔曲线，每种都BezierPath和CAShaperLayer结合画一次
 拓展了两个，一个是进度条，另外一个是Loading动画
 */
- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    switch (_type) {
        case 0: // 矩形
        {
            [self drawRect];
        }
            break;
           
        case 1: // 圆
        {
            [self drawCircle];
        }
            break;
            
        case 2: // 进度条
        {
            [self drawProgress];
        }
            break;
            
        case 3: // Loading动画
        {
            [self drawLoading];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - 画矩形

- (CAShapeLayer *)drawRect
{
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    // 指定frame，只是为了设置宽度和高度
    circleLayer.frame = CGRectMake(0, 0, 100, 200);
    // 设置居中显示
    circleLayer.position = self.view.center;
    // 设置填充颜色
    circleLayer.fillColor = [UIColor greenColor].CGColor;
    // 设置线宽
    circleLayer.lineWidth = 2.0;
    // 设置线的颜色
    circleLayer.strokeColor = [UIColor redColor].CGColor;
    
    // 使用UIBezierPath创建路径
    CGRect frame = CGRectMake(0, 0, 100, 200);
    UIBezierPath *rectPath = [UIBezierPath bezierPathWithRect:frame];
    
    // 设置CAShapeLayer与UIBezierPath关联
    circleLayer.path = rectPath.CGPath;
    
    // 将CAShaperLayer放到某个层上显示
    [self.view.layer addSublayer:circleLayer];
    
    return circleLayer;
}

#pragma mark - 画圆

- (CAShapeLayer *)drawCircle
{
    CAShapeLayer *circleLayer = [CAShapeLayer layer];
    // 指定frame，只是为了设置宽度和高度
    circleLayer.frame = CGRectMake(0, 0, 200, 200);
    // 设置居中显示
    circleLayer.position = self.view.center;
    // 设置填充颜色
    circleLayer.fillColor = [UIColor clearColor].CGColor;
    // 设置线宽
    circleLayer.lineWidth = 2.0;
    // 设置线的颜色
    circleLayer.strokeColor = [UIColor redColor].CGColor;
    
    // 使用UIBezierPath创建路径
    CGRect frame = CGRectMake(0, 0, 200, 200);
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithOvalInRect:frame];
    
    // 设置CAShapeLayer与UIBezierPath关联
    circleLayer.path = circlePath.CGPath;
    
    // 将CAShaperLayer放到某个层上显示
    [self.view.layer addSublayer:circleLayer];
    
    return circleLayer;
}

#pragma mark - 进度条

- (void)drawProgress
{
    CAShapeLayer *grayLayer =[CAShapeLayer layer];
    UIBezierPath *grayPath = [UIBezierPath bezierPathWithArcCenter:self.view.center
                                                            radius:60
                                                        startAngle:M_PI * 3 / 2
                                                          endAngle:M_PI * 7 / 2
                                                         clockwise:YES];
    grayLayer.path = grayPath.CGPath;
    grayLayer.strokeColor = [UIColor grayColor].CGColor;
    grayLayer.fillColor = [UIColor clearColor].CGColor;
    grayLayer.lineWidth = 3;
    [self.view.layer addSublayer:grayLayer];
    
    grayLayer.strokeStart  = 0;
    grayLayer.strokeEnd = 1;
    
    CAShapeLayer *greenLayer = [CAShapeLayer layer];
    UIBezierPath *greenPath = [UIBezierPath bezierPathWithArcCenter:self.view.center
                                                             radius:60
                                                         startAngle:M_PI * 3 / 2
                                                           endAngle:M_PI * 7 / 2
                                                          clockwise:YES];
    greenLayer.path = greenPath.CGPath;
    greenLayer.strokeColor = [UIColor greenColor].CGColor;
    greenLayer.fillColor = [UIColor clearColor].CGColor;
    greenLayer.lineWidth = 3;
    [self.view.layer addSublayer:greenLayer];

    greenLayer.strokeStart = 0;
    greenLayer.strokeEnd = 1;

    // 添加动画
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    animation.duration = 3.0;
    animation.fromValue = @(0);
    animation.toValue = @(1);
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    [greenLayer addAnimation:animation forKey:nil];
}

#pragma mark - Loading动画

- (void)drawLoading
{
    UIView *loadingView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-50, self.view.frame.size.height/2-50, 100, 100)];
    [self.view addSubview:loadingView];
    
    // 外层旋转动画
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.fromValue = @0.0;
    rotationAnimation.toValue = @(2 * M_PI);
    rotationAnimation.repeatCount = HUGE_VALF;
    rotationAnimation.duration = 3.0;
    rotationAnimation.beginTime = 0.0;
    
    [loadingView.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    
    // 内层进度条动画
    CABasicAnimation *strokeAnim1 = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    strokeAnim1.fromValue = @0.0;
    strokeAnim1.toValue = @1.0;
    strokeAnim1.duration = 1.0;
    strokeAnim1.beginTime = 0.0;
    strokeAnim1.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    // 内层进度条动画
    CABasicAnimation *strokeAnim2 = [CABasicAnimation animationWithKeyPath:@"strokeStart"];
    strokeAnim2.fromValue = @0.0;
    strokeAnim2.toValue = @1.0;
    strokeAnim2.duration = 1.0;
    strokeAnim2.beginTime = 1.0;
    strokeAnim2.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CAAnimationGroup *animGroup = [CAAnimationGroup animation];
    animGroup.duration = 2.0;
    animGroup.repeatCount = HUGE_VALF;
    animGroup.fillMode = kCAFillModeForwards;
    animGroup.animations = @[strokeAnim1, strokeAnim2];
    
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(3, 3, CGRectGetWidth(loadingView.frame)-3*2, CGRectGetHeight(loadingView.frame)-3*2)];
    self.loadingLayer = [CAShapeLayer layer];
    self.loadingLayer.lineWidth = 3;
    self.loadingLayer.lineCap = kCALineCapRound;
    self.loadingLayer.strokeColor = [UIColor greenColor].CGColor;
    self.loadingLayer.fillColor = [UIColor clearColor].CGColor;
    self.loadingLayer.strokeStart = 0.0;
    self.loadingLayer.strokeEnd = 1.0;
    self.loadingLayer.path = path.CGPath;
    [self.loadingLayer addAnimation:animGroup forKey:@"strokeAnim"];
    
    [loadingView.layer addSublayer:self.loadingLayer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
