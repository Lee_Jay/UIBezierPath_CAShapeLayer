//
//  ShapeLayerDetailViewController.h
//  UIBezierPath和CAShapeLayer绘图
//
//  Created by LeeJay on 16/9/28.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShapeLayerDetailViewController : UIViewController

@property (nonatomic, assign) NSInteger type;

@end
