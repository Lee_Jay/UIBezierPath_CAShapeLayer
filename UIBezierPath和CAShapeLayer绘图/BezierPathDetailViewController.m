//
//  BezierPathDetailViewController.m
//  UIBezierPath和CAShapeLayer绘图
//
//  Created by LeeJay on 16/9/28.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import "BezierPathDetailViewController.h"
#import "BezierPathDetailView.h"

@interface BezierPathDetailViewController ()

@end

@implementation BezierPathDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
}

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    BezierPathDetailView *view = [[BezierPathDetailView alloc] initWithFrame:CGRectMake(0, 100, self.view.frame.size.width, height - 140 - 64)];
    [self.view addSubview:view];
    view.layer.borderColor = [UIColor redColor].CGColor;
    view.layer.borderWidth = 5;
    view.backgroundColor = [UIColor whiteColor];
    view.type = _bezierType;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
