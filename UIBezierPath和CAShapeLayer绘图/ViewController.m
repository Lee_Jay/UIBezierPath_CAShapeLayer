//
//  ViewController.m
//  UIBezierPath和CAShapeLayer绘图
//
//  Created by LeeJay on 16/9/26.
//  Copyright © 2016年 Mob. All rights reserved.
//

#import "ViewController.h"
#import "BezierPathViewController.h"
#import "ShapeLayerViewController.h"

@interface ViewController ()

@property (nonatomic, copy) NSArray *datas;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.datas = @[@"UIBezierPath",@"CAShapeLayer"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.datas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellId = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.textLabel.text = self.datas[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0)
    {
        BezierPathViewController *bezierPath = [[BezierPathViewController alloc] init];
        bezierPath.title = self.datas[indexPath.row];
        [self.navigationController pushViewController:bezierPath animated:YES];
    }
    else
    {
        ShapeLayerViewController *shape = [[ShapeLayerViewController alloc] init];
        shape.title = self.datas[indexPath.row];
        [self.navigationController pushViewController:shape animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
